import React from 'react';

class ConferenceForm extends React.Component {
    //Need a constructor for timing issue
    //When component first loads, there is no array of conferences
    //in the component's state. When it tries to render the first
    //time, the value of this.state.conferences is empty, so we need to
    //give it a default value.
    constructor(props) {
        //Need to call the constructor on the parent class, so we use the super keyword
        super(props)
        //Now we add the default state
        //this.state = new dictionary with the states key and an empty list
        //set an initial state for the input tags
        this.state = {
            name: '',
            starts: '',
            ends: '',
            description: '',
            maxPresentations: '',
            maxAttendees: '',
            locations: []
        };

        //If you want to use a method in the class as an event handler, you need to
        //bind in the constructor. Otherwise, if you have an object and you refer to one
        //of its methods like a property, JavaScript forgets what object it came from.
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleDateStartsChange = this.handleDateStartsChange.bind(this);
        this.handleDateEndsChange = this.handleDateEndsChange.bind(this);
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
        this.handleMaxPresentationsChange = this.handleMaxPresentationsChange.bind(this);
        this.handleMaxAttendeesChange = this.handleMaxAttendeesChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    //To submit the form, use the same kind of event handler, but onSubmit instead of onChange
    async handleSubmit(event) {
        event.preventDefault();
        //copies all of the properties and values from this.state into a new object called data
        const data = {...this.state};
        //creating a new key named "max_attendees" and setting it to the value of "maxAttendees"
        //state is maxAttendees, but expected JSON is max_attendees
        data.max_attendees = data.maxAttendees;
        //creating a new key named "max_presentations" and setting it to the value of "maxPresentations"
        //state is maxPresentations, but expected JSON is max_presentations
        data.max_presentations = data.maxPresentations;
        //Deleting maxAttendees, maxPresentations, and locations
        //since we do not want to send it to the server
        delete data.maxAttendees;
        delete data.maxPresentations;
        delete data.locations;
        console.log("data is:",data);

        //This is the POST code from new-conference.js
        //create variable to store url for create conferences API
        const conferenceUrl = "http://localhost:8000/api/conferences/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        //Get the data back. fetch the URL using fetchConfigs. 
        //Use the await keyword so we get the response, not the Promise
        const response = await fetch(conferenceUrl, fetchConfig);

        //If the response is ok
        if(response.ok) {
            //get the data using the .json method, use await keyword
            const newConference = await response.json();
            console.log("newConference is:", newConference);

            //Form isn't cleared on successful save, need to set the state to empty strings
            // and use those values for the form elements.
            //corresponds with the value attributes of input tags, select tag,etc. in JSX
            const cleared = {
                name: '',
                starts: '',
                ends: '',
                description: '',
                maxPresentations: '',
                maxAttendees: '',
                location: '',
            };
            this.setState(cleared);
        }
    }

    //Need to keep track of the state of the different inputs, so when people type in 
    // the different input fields, we can get the data into the state of the component
    //We added the onChange attribute to the input tag that handles the location's name.
    //So, we now need a method for this
    //The event parameter is the event that occured
    handleNameChange(event) {
        //The target property on it is the HTML tag that caused the event
        //In this case, the event.target is the input for the conference's name
        const value = event.target.value;
        //Then the value property is the value in the input
        this.setState({name: value})
    }

    handleDateStartsChange(event) {
        const value = event.target.value;
        this.setState({starts: value})
    }

    handleDateEndsChange(event) {
        const value = event.target.value;
        this.setState({ends: value})
    }

    handleDescriptionChange(event) {
        const value = event.target.value;
        this.setState({description: value})
    }

    handleMaxPresentationsChange(event) {
        const value = event.target.value;
        this.setState({maxPresentations: value})
    }

    handleMaxAttendeesChange(event) {
        const value = event.target.value;
        this.setState({maxAttendees: value})
    }

    handleLocationChange(event) {
        const value = event.target.value;
        this.setState({location: value})
    }

    async componentDidMount() {
        //Url for List Locations
        const url = "http://localhost:8000/api/locations/";

        const response = await fetch(url);

        if(response.ok) {
            const data = await response.json();
            //Add data to the state of the ConferenceForm component
            //pass in a dictionary that has the key 'locations' as its value
            this.setState({locations: data.locations});
        }
    }

    render() {
        return (
            <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create a new conference</h1>
                <form onSubmit={this.handleSubmit} id="create-conference-form">
                  <div className="form-floating mb-3">
                    <input onChange={this.handleNameChange} value={this.state.name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                    <label htmlFor="name">Name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleDateStartsChange} value={this.state.starts} required type="date" name="starts" id="starts" className="form-control" />
                    <label htmlFor="starts">Starts</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleDateEndsChange} value={this.state.ends} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control" />
                    <label htmlFor="ends">Ends</label>
                  </div>
                  <div className="mb-3">
                    <label htmlFor="Description">Description</label>
                    <textarea onChange={this.handleDescriptionChange} value={this.state.description} className="form-control" required type="text" id="description" rows="3"></textarea>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleMaxPresentationsChange} value={this.state.maxPresentations} placeholder="Maximum presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control" />
                    <label htmlFor="Maximum presentations">Maximum presentations</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleMaxAttendeesChange} value={this.state.maxAttendees} placeholder="Maximum attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control" />
                    <label htmlFor="Maximum attendees">Maximum attendees</label>
                  </div>
                  <div className="mb-3">
                    <select onChange={this.handleLocationChange} value={this.state.location} required id="location" className="form-select" name="location">
                        <option value="">Choose a location</option>
                            {this.state.locations.map(location => {
                                return (
                                    <option key= {location.href} value={location.id}>
                                        {location.name}
                                    </option>
                                );
                            })}
                    </select>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
        );
    }
}

export default ConferenceForm;
