import React from 'react';

class LocationForm extends React.Component {
    //need a constructor for timing issue "cannot read properties of null (reading 'states')"
    //When component first loads, there is no array of U.S. states
    //in the component's state. When it tries to render the first
    //time, the value of this.state.states is empty, so we need to
    //give it a default value.
    constructor(props) {
        //Need to call the constructor on the parent class, so we use the super keyword
        super(props)
        //Now we add the default state
        //this.state = new dictionary with the states key and an empty list
        //set an initial state for the input tags
        this.state = {
            name: '',
            roomCount: '',
            city: '',
            states: []
        };
        //If you want to use a method in the class as an event handler, you need to
        //bind in the constructor. Otherwise, if you have an object and you refer to one
        //of its methods like a property, JavaScript forgets what object it came from.
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleRoomCountChange = this.handleRoomCountChange.bind(this);
        this.handleCityChange = this.handleCityChange.bind(this);
        this.handleStateChange = this.handleStateChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    //To submit the form, use the same kind of event handler, but onSubmit instead of onChange
    //If you look at state, property named roomCount, camel case. JSON expected is room_count, snake case.
    //corresponds with <form onSubmit = {this.handleSubmit}> in JSX
    async handleSubmit(event) {
        event.preventDefault();
        //copies all of the properties and values from this.state into a new object called data
        // ... is an easy way to make a copy of objects in JavaScript
        const data = {...this.state};
        //creating a new key named "room_count" and setting it to the value of "roomCount"
        data.room_count = data.roomCount;
        //deleting the keys "roomCount" and "states" because we don't want to send that 
        //data to the server
        delete data.roomCount;
        delete data.states;
        console.log(data);

        //This is the POST code from new-location.js
        //create variable to store url for create locations API
        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        //Get the data back. fetch the URL using fetchConfigs. 
        //Use the await keyword so we get the response, not the Promise
        const response = await fetch(locationUrl, fetchConfig);
        //If the response is ok
        if (response.ok) {
        //get the data using the .json method, use await keyword
          const newLocation = await response.json();
          console.log(newLocation);
        //Form isn't cleared on successful save, need to set the state to empty strings
        // and use those values for the form elements.
        //corresponds with the value attributes of input tags and select tag in JSX
          const cleared = {
              name: '',
              roomCount: '',
              city: '',
              state: '',
          };
          this.setState(cleared);
        }
    }

    //Need to keep track of the state of the different inputs, so when people type in 
    // the different input fields, we can get the data into the state of the component
    //We added the onChange attribute to the input tag that handles the location's name.
    //So, we now need a method for this
    //The event parameter is the event that occured
    handleNameChange(event) {
        //The target property on it is the HTML tag that caused the event
        //In this case, the event.target is the input for the location's name
        const value = event.target.value;
        //Then the value property is the value in the input
        this.setState({name: value})
    }

    handleRoomCountChange(event) {
        const value = event.target.value;
        this.setState({roomCount: value})
    }

    handleCityChange(event) {
        const value = event.target.value;
        this.setState({city: value})
    }

    handleStateChange(event) {
        const value = event.target.value;
        this.setState({state: value})
    }




    async componentDidMount() {
        //Url for Getting list of states
        const url = 'http://localhost:8000/api/states/';
    
        const response = await fetch(url);
    
        if (response.ok) {
          const data = await response.json();
          //Add data to the state of the LocationForm component
          //pass in a dictionary that has the key 'states' as its value
          this.setState({states: data.states});
    
        }
      }

    render() {
      return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new location</h1>
              <form onSubmit={this.handleSubmit} id="create-location-form">
                <div className="form-floating mb-3">
                  <input onChange={this.handleNameChange} value={this.state.name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                  <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange = {this.handleRoomCountChange} value={this.state.roomCount} placeholder="Room count" required type="number" name="room_count" id="room_count" className="form-control" />
                  <label htmlFor="room_count">Room count</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={this.handleCityChange} value={this.state.city} placeholder="City" required type="text" name="city" id="city" className="form-control" />
                  <label htmlFor="city">City</label>
                </div>
                <div className="mb-3">
                  <select onChange={this.handleStateChange} value={this.state.state} required name="state" id="state" className="form-select">
                    <option value="">Choose a state</option>
                    {/* loop over the values in the states array inside the JSX in the render method */}
                    {/* React badly wants a unique key value, so abbreviation is guaranteed to be unique as seen in the State model class */}
                    {/* Similar to what we did for the AttendeesList function component */}
                    {this.state.states.map(state => {
                        return (
                        <option key= {state.abbreviation} value={state.abbreviation}>
                            {state.name}
                        </option>
                        );
                    })}
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      );
    }
  }

export default LocationForm;