function createLocation(currentLocationName, currentLocationId){
    return `
    <option value=${currentLocationId}>${currentLocationName}</option>
    `
}


//Add event listener for when the DOM loads
window.addEventListener('DOMContentLoaded', async () => {
    //declare variable to hold the URL for the RESTful API
    const url = 'http://localhost:8000/api/locations/';
    //Get the data back. fetch the url. Use await keyword so we get the response, not the Promise
    const response = await fetch(url);
    //Create an empty array which we will use to push our location items
    const elementsToRender = [];


    //if the response is ok
    if (response.ok) {
        //get the data using the .json method, use await keyword
        const data = await response.json();
        //console.log(data);
        //Get the select tag element by its id 'location'
        const selectTag = document.getElementById('location');


        for (let location of data.locations){
            const currentLocationName = location.name;
            const currentLocationId = location.id;
            //Create an option element and set it equal to our createLocation function
            // that we made earlier. 
            //Its arguments are the locationName and locationId we already got
            const option = createLocation(currentLocationName, currentLocationId);
            elementsToRender.push(option);
        }
        selectTag.innerHTML = elementsToRender.join("");
    //references the form in new-conference.html 
        const formTag = document.getElementById('create-conference-form');

        formTag.addEventListener('submit', async (event) => {
            event.preventDefault();
            //console.log('need to submit the form data');
            //FormData object expects name attributes on the form inputs. Need
            //to add them to the input and select tags in new-conference.html 
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData.entries()));
            //console.log(json);

            const conferenceUrl = 'http://localhost:8000/api/conferences/';
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const response = await fetch(conferenceUrl, fetchConfig);

            if (response.ok) {
                //reset() will reset the form to its original state.
                //In our case it should just clear it so we know if
                //something happened. 
                formTag.reset();

                const newConference = await response.json();

                //const responseJson = await response.json();
                const responseJson = newConference;

                console.log("newConference:", newConference);
            }
        })
    }
})