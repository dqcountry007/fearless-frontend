window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');
  
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();
  
        for (let conference of data.conferences) {
            const option = document.createElement('option');
            option.value = conference.href;
            option.innerHTML = conference.name;
            selectTag.appendChild(option);
        }

        //Here, add the 'd-none' class to the loading icon
        const loadingIcon = document.getElementById('loading-conference-spinner')
        const addClass = loadingIcon.classList.add("d-none");
        //Here, remove 'd-none' class from the select tag
        const removeClass = selectTag.classList.remove("d-none");

        //get the attendee form element by its id
        const formTag = document.getElementById("create-attendee-form");

        //add an event handler for the submit event
        formTag.addEventListener('submit', async (event) => {
            //prevent the default from happening
            event.preventDefault();

            //Create a FormData object from the form
            const formData = new FormData(formTag);

            //Create a new object from the form data's entries
            const json = JSON.stringify(Object.fromEntries(formData.entries()));
            console.log("json is:", json);

            //Create options for the fetch
            const attendeesUrl = 'http://localhost:8001/api/attendees/'; 
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type':'application/json',
                },
            };

            const response = await fetch(attendeesUrl,fetchConfig);
            //When the response from the fetch is good
            if(response.ok){
                //Reset the form
                formTag.reset();
                const newAttendee = await response.json();
                const responseJson = newAttendee;
                console.log("newAttendee:", newAttendee);


                //Grab the id of the success message
                const successAlert = document.getElementById('success-message');
                //Remove the d-none from the success alert
                const rmvDNone = successAlert.classList.remove("d-none");
                //Add d-none to the form
                const addFormDNone = formTag.classList.add("d-none");
            }
        })
    }
 })
