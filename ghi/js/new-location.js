//Add event listener for when the DOM loads
window.addEventListener('DOMContentLoaded', async () => {
    //declare variable to hold the URL for the RESTful API that we just created
    const url = 'http://localhost:8000/api/states/';
    //Get the data back. fetch the url. Use await keyword so we get the response, not the Promise
    const response = await fetch(url);
    //if the response is ok
    if (response.ok) {
        //get the data using the .json method, use await keyword
        const data = await response.json();
        console.log(data);
        //Get the select tag element by its id 'state'
        const selectTag = document.getElementById('state');
        for (let state of data.states){
            //Create an option element
            const option = document.createElement('option');
            //Set the '.value' property of the option element to the 
            //state's abbreviation
            option.value = state.abbreviation;
            //Set the '.innerHTML' property of the option element to
            //the state's name
            option.innerHTML = state.name;
            //Append the option element as a child of the select tag
            selectTag.appendChild(option);
        }
        console.log(data);
    }

    const formTag = document.getElementById('create-location-form');

    formTag.addEventListener('submit', async (event) => {
        event.preventDefault();
        //console.log('need to submit the form data');
        //FormData object expects name attributes on the form inputs. Need
        //to add them to the input and select tags in new-location.html 
        //Using FormData object to read the data from the form.
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData.entries()));
        //const json = JSON.stringify(Object.fromEntries(formData));
        //console.log(json);

        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            //reset() will reset the form to its original state.
            //In our case it should just clear it so we know if
            //something happened. 
            formTag.reset();

            const newLocation = await response.json();

            //const responseJson = await response.json();
            const responseJson = newLocation;

            //console.log("newLocation:", newLocation);
        }
    });
});

