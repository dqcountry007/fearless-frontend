// Get the cookie out of the cookie store
//get() method gets a single cookie with the given name or options object,
// it returns a Promise that resolves with details of a single cookie
const payloadCookie = await cookieStore.get('jwt_access_payload');
if (payloadCookie) {
  // The cookie value is a JSON-formatted string, so parse it
  //JSON.parse() parses a JSON string, constructing the JS value or
  // object described by the string. 
  const encodedPayload = JSON.parse(payloadCookie.value);

  // Convert the encoded payload from base64 to normal string
  // atob() decodes a string of data which has been encoded using Base64
  // encoding.
  const decodedPayload = atob(encodedPayload);

  // The payload is a JSON-formatted string, so parse it
  const payload = JSON.parse(decodedPayload);

  // Print the payload
  console.log(payload);

  const permissions = payload.user.perms;
  // Check if "events.add_conference" is in the permissions.
    if(permissions.includes("events.add_conference")){
        //If it is, remove 'd-none' from the link
        const linkTag = document.getElementById('conferenceLink');
        linkTag.classList.remove("d-none");
    }

  // Check if "events.add_location" is in the permissions.
    if(permissions.includes("events.add_location")){
        //If it is, remove 'd-none' from the link
        const locLinkTag = document.getElementById('locationLink');
        locLinkTag.classList.remove("d-none");
    }
  
  // Check if "presentations.add_presentation" is in the permissions.
    if(permissions.includes("presentations.add_presentation")) {
      //If it is, remove 'd-none' from the link
      const presentationLinkTag = document.getElementById("newPresentationNavLink");
      presentationLinkTag.classList.remove("d-none");
    }
}

