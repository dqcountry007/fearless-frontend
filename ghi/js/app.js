function createCard(name, description, pictureUrl, startDate, endDate, location) {
    return `
      <div class="card w-25 shadow p-3 mb-5 bg-white rounded" style="margin:25px; ">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
          </div>
          <div class="card-footer">${startDate} to ${endDate}</div>
        </div>
      </div>
    `;
  }

//Create an alert function to use in EventListener
function alertUser(errorAlert){
    return `
    <div class="alert alert-danger" role="alert">An error has occurred. 
    The error you received is: ${errorAlert}
    </div>
    `;
}

window.addEventListener('DOMContentLoaded', async () => {
//cannot use await unless the function is an async function ^ 

    //This is the url for List conferences
    const url = 'http://localhost:8000/api/conferences/';

    try {
        //fetch function returns a Promise. Need to use await before it to handle the Promise.
        const response = await fetch(url);
        //console.log(response);
        if (!response.ok) {
            //Figure out what to do when the response is bad
            throw new Error("Response not ok");

        } else {
            //response.json() return value is a Promise object. 
            //Again, use await before it because it is a Promise being returned,
            //We do not want the Promise, but the value the Promise will turn into after you await it.  
            //data contains the info for the conferences 
            const data = await response.json();
            //console.log(data);

            for (let conference of data.conferences) {
                //This is the url for conference details
                const detailUrl = `http://localhost:8000${conference.href}`;

                const detailResponse = await fetch(detailUrl);

                if (detailResponse.ok) {

                    const details = await detailResponse.json();
                    //console.log(details);

                    const name = details.conference.name;

                    const description = details.conference.description;
                
                    const pictureUrl = details.conference.location.picture_url;

                    const starts = details.conference.starts;
                    const startDate = new Date(starts).toLocaleDateString('en-US', {year:"numeric", month: "short", day:"numeric"});
                    //const startsFormatted = startDate.getMonth();
                    console.log("startDate is: ", startDate);

                    const ends = details.conference.ends;
                    const endDate = new Date(ends).toLocaleDateString('en-US', {year:"numeric", month: "short", day:"numeric"});
                    console.log("endDate is: ", endDate);

                    const location = details.conference.location.name;
                    
                    const html = createCard(name, description, pictureUrl, startDate, endDate, location);
                    //console.log("html is:",html);

                    const column = document.querySelector('.row');
                    column.innerHTML += html;
                }
            }
        
        }
    }   catch (error) {
        console.error("error is:", error);
        //Figure out what to do if an error is raised
        const errorHtml = alertUser(error.errorAlert);
        //grab all the .row class selectors
        const row = document.querySelector('.row');
        //populate it with the error message 
        row.innerHTML = errorHtml + row.innerHTML;
    }

});
