//
function createConferenceOption(conferenceName, currentConferenceId) {
    return `
    <option value=${currentConferenceId}>${conferenceName}</option>
    `
  }
  //adds the current conference id into the create presentation url
  function createUrl(id) {
    return `http://localhost:8000/api/conferences/${id}/presentations/`
  }
  
  window.addEventListener('DOMContentLoaded', async () => {
    const conferenceUrl = "http://localhost:8000/api/conferences/";
  
    try {
        //Get the data back, fetch the url, use await so we get the response
      const response = await fetch(conferenceUrl);
      const elementsToRender = [];
  
      if (response.ok) {
          //get the data using the .json method, use await keyword
        const data = await response.json();
        //Get the select tag element by its id of 'conference'
        const selectTag = document.getElementById('conference');

        for (let index in data.conferences) {
          const currentConferenceName = data.conferences[index].name;
          const currentHref = data.conferences[index].href;

          // the '/\d+/' is a regular expression that matches by the digit (d) 
          // and checks that all of them match and not just one (+ quantifier)
          const currentConferenceId = currentHref.match(/\d+/)[0];
           //Create an option variable and set it equal to our function from above
           //Arguments are the variables we already got in the for loop
          const option = createConferenceOption(currentConferenceName, currentConferenceId);
            //Push it to the array
          elementsToRender.push(option);
        }
        selectTag.innerHTML = elementsToRender.join("");
        //references the form in new-presentation.html
        const formTag = document.getElementById('create-presentation-form');
        formTag.addEventListener('submit', async event => {
          event.preventDefault();
            //FormData object expects Conference name attributes on the form inputs
            //Need to add them to the input and select tags in new-presentation.html
          const formData = new FormData(formTag);
          //get the id of the current conference 
          const currentConferenceId = formData.get('conference');
            //presentationURL = set it equal to our function
            //feed it the id of the current conference
          const presentationUrl = createUrl(currentConferenceId);

          formData.delete('conference');

          let json = JSON.stringify(Object.fromEntries(formData));
          const fetchConfig = {
            method: "post",
            body: json,
            headers: {
              'Content-Type': 'application/json',
            },
          };
          const response = await fetch(presentationUrl, fetchConfig);
          if (response.ok) {
              //Reset the form to its original state
            formTag.reset();
            const newPresentation = await response.json();
          }
  
        });
  
      }
  
    } catch (error) {
      console.error('error', error)
    }
  })
